# [`terraform-validate`](./terraform-validate.yml)

Runs [`terraform validate`](https://www.terraform.io/cli/commands/validate) to ensure that all Terraform files are valid.

1. Ensure that the `GL_ASDF_TERRAFORM_VERSION` version for terraform is configured.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_TERRAFORM_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Ensures that all terraform files are syntactically valid
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/terraform-validate.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: terraform-validate.yml
```


# [`tflint`](./tflint.yml)

Runs [`tflint`](https://github.com/terraform-linters/tflint) across all directories that contain `*.tf` files.

Setup process:

1. Ensure that a [`.tflint.hcl`](https://github.com/terraform-linters/tflint/blob/master/docs/user-guide/config.md) file exists in the root of the project.
1. Ensure that the `GL_ASDF_TERRAFORM_VERSION` version for terraform is configured.
1. Ensure that the `GL_ASDF_TFLINT_VERSION` version for tflint is configured.
1. Directories can be excluded from tflint using the `TFLINT_EXCLUDE_REGEX` variable. See the example below.
1. The task will generate a [junit test output file](https://docs.gitlab.com/ee/ci/unit_test_reports.html) for any failed linter checks.
1. Supports the `COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP` variable, for excluding paths from validation.

```yaml
stages:
  - validate

# Not needed if .gitlab-ci-asdf-versions.yml is included...
variables:
  GL_ASDF_TFLINT_VERSION: ...
  GL_ASDF_TERRAFORM_VERSION: ...
  # Exclude vendor and files directories from validation
  COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP: '^\.(/vendor/|/files/)'

include:
  # Not required, but recommended
  - local: .gitlab-ci-asdf-versions.yml

  # Runs tflint on all terraform module directories
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/tflint.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: tflint.yml
```

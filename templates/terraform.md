# [`templates/terraform.yml`](./templates/terraform.yml)

This template should be used for Terraform projects. It performs standard validity checks against the Terraform files in the project.

Ensure that the project has a valid [`.tflint.hcl`](https://github.com/terraform-linters/tflint/blob/v0.34.1/docs/user-guide/config.md) file in the root directory.

Includes these tasks:

1. [`terraform-format.yml`](../terraform-format.md): runs [`terraform fmt`](https://www.terraform.io/cli/commands/fmt) to ensure that all Terraform files are correctly formatted.
1. [`terraform-validate.yml`](../terraform-validate.md): runs [`terraform validate`](https://www.terraform.io/cli/commands/validate) to ensure that all Terraform files are valid.
1. [`tflint.yml`](../tflint.md): runs [`tflint`](https://github.com/terraform-linters/tflint) across all directories that contain `*.tf` files.
1. [`checkov.yml`](../checkov.md): runs [`checkov`](https://www.checkov.io/) across the project.

```yaml
# Requires validate stage
stages:
  - validate

# Better to define these through .gitlab-ci-asdf-versions.yml
variables:
  GL_ASDF_TERRAFORM_VERSION: ...
  GL_ASDF_TFLINT_VERSION: ...
  GL_ASDF_CHECKOV_VERSION: ...

include:
  - local: .gitlab-ci-asdf-versions.yml
  # Runs Terraform validations, including tflint, terraform validate and terraform formatting checks
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/terraform.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v1.61.0  # renovate:managed
    file: templates/terraform.yml
```

 [`templates/golang.yml`](./templates/golang.yml)

This template should be used for Golang projects.

Includes the following tasks:

1. [`go-mod-tidy.yml`](../go-mod-tidy.md): ensures that [`go mod tidy`](https://go.dev/ref/mod) is up-to-date and `go.mod` and `go.sum` are tidy.
1. [`go-unittests.yml`](../go-unittests.md): runs unit tests to ensure and emits a JUnit XML report for GitLab.
1. [`golangci-lint.yml`](../golangci-lint.md): runs [golangci-lint](https://github.com/golangci/golangci-lint) on the project.
1. [`goreleaser.yml`](../goreleaser.md): builds a binary release of the project using [GoReleaser](https://goreleaser.com/).

```yaml
# Requires stages validate and release
stages:
  - validate
  - release

# Better to define these through .gitlab-ci-asdf-versions.yml
variables:
  GL_ASDF_GOLANG_VERSION: ...
  GL_ASDF_GOLANGCI_LINT_VERSION: ...
  GL_ASDF_GORELEASER_VERSION: ...

include:
  - local: .gitlab-ci-asdf-versions.yml
  # Runs golang standard tests, including tests, goreleaser, golangci-lint and go-mod-tidy
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/templates/golang.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: templates/golang.yml
```

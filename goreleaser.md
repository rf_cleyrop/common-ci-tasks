
# [`goreleaser`](./goreleaser.yml)

Including this template will build a binary release of the project using [GoReleaser](https://goreleaser.com/).

It runs on tag pipelines.

It requires a `.goreleaser.yml` file in the root of the project, which looks something like this:

* Default Stages: `validate`, `release`

```
before:
  hooks:
    - go mod tidy
builds:
  - id: "<binaryname>"
    binary: "<binaryname>"
    env:
      - CGO_ENABLED=0
    goos:
      - linux
      - darwin
    goarch:
      - amd64
      - arm64
    ldflags:
      - -s -w -X '{{.ModulePath}}/cmd.version={{.Version}}' -X '{{.ModulePath}}/cmd.commit={{.Commit}}' -X '{{.ModulePath}}/cmd.date={{.Date}}'

archives:
  - replacements:
      darwin: Darwin
      linux: Linux
      amd64: x86_64

dockers:
  - image_templates:
    - "{{ .Env.CI_REGISTRY_IMAGE }}:latest"
    - "{{ .Env.CI_REGISTRY_IMAGE }}:{{ .Tag }}"
    - "{{ .Env.CI_REGISTRY_IMAGE }}:v{{ .Major }}"
    goos: linux
    goarch: amd64

release:
  gitlab:
    owner: gitlab-com/gl-infra # GitLab Group
    name: <projectname> # GitLab Project

checksum:
  name_template: 'checksums.txt'

snapshot:
  name_template: "{{ .Tag }}-next"

changelog:
  sort: asc
  filters:
    exclude:
      - '^docs:'
      - '^test:'

gitlab_urls:
  api: '{{ .Env.CI_SERVER_URL }}'
  download: '{{ .Env.CI_SERVER_URL }}'
```

Next, create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api`  and make it available in your CI environment via the `GITLAB_TOKEN` environment variable through the CI/CD Variables settings.

```yaml
stages:
  - validate
  - release

variables:
  GL_ASDF_GORELEASER_VERSION: ...

include:
  # build binary release artifacts with goreleaser
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: goreleaser.yml
```

## Options

Additional arguments (such as passing environment variables to be consumed by goreleaser) can be set using the `GORELEASER_DOCKER_EXTRA_ARGS` variable.

Additional command line options for Goreleaser can be set using the `GORELEASER_EXTRA_ARGS` argument. These will be appended to the arguments for `goreleaser release`.

## FIPS Mode

If you wish to use `goreleaser` to build your project in a way that is FIPS compliant, you will need to specify the following variables when using the `goreleaser` tasks from this repository:

* `FIPS_MODE`: This instructs the CI pipeline to use the gorelease job specifically for creating FIPS compliant releases
* `GL_ASDF_GORELEASER_VERSION`: The version of goreleaser to use, minimum 1.13.1
* `GL_ASDF_GOLANG_VERSION`: The version of golang to use to compile, minimum 1.19

An example

```yaml
stages:
  - validate
  - release

variables:
  GL_ASDF_GORELEASER_VERSION: ...
  GL_ASDF_GOLANG_VERSION: ...
  FIPS_MODE: 1

include:
  # build binary release artifacts with goreleaser
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/goreleaser.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: goreleaser.yml
```

### I'm seeing `failed to pull image` errors...

In order to generate FIPS compliant images, the Goreleaser task uses [Goreleaser's cross-compilation images](https://goreleaser.com/cookbooks/cgo-and-crosscompiling/). These images use a combination of the Golang version (`GL_ASDF_GOLANG_VERSION`) and Goreleaser version (`GL_ASDF_GORELEASER_VERSION`) for performing cross-compilation. Not all combinations are supported.

If you see errors such as `ERROR: Job failed: failed to pull image "goreleaser/goreleaser-cross:v1.20.1-v1.14.1"` when running the validation step, this probably means there is no combination of Golang version/Goreleaser version.

Review the tags for the `goreleaser/goreleaser-cross` image on Docker Hub to find a valid combination: <https://hub.docker.com/r/goreleaser/goreleaser-cross/tags>.

## Out-of-Memory Issues

If you're experiencing OOM kills during the Goreleaser build, consider using a bigger runner by updating the `tags` on the `goreleaser` job. It may also help to reduce paralellism in the Goreleaser build tool.

This can be done as follows:

```yaml
goreleaser:
  tags: [saas-linux-medium-amd64]
  variables:
    GORELEASER_EXTRA_ARGS: "-p 1"
```


# [`semantic-release`](./semantic-release.yml)

This job will run [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/) on your repository. This allows the repository to be automatically tagged based on [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) messages.

Further Reading:

1. [Automate Semantic Versioning with Conventional Commits](https://medium.com/@jsilvax/automate-semantic-versioning-with-conventional-commits-d76a9f45f2fa).
1. [Basic Guide to Semantic Release](https://levelup.gitconnected.com/basic-guide-to-semantic-release-9e2aa7834e4b)

* Default Stage: `release` (make sure this stage is one of the last in your pipeline)

You will also need a file called `.releaserc.json` included in your project. Copy this one if you are unsure.

```json
{
  "branches": ["main"],
  "plugins": [
    [
      "@semantic-release/commit-analyzer",
      {
        "preset": "conventionalcommits"
      }
    ],
    [
      "@semantic-release/release-notes-generator",
      {
        "preset": "conventionalcommits"
      }
    ],
    "@semantic-release/gitlab"
  ]
}
```

Next, create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api` and `write_repository` scope and make it available in your CI environment via the `SEMANTIC_RELEASE_GITLAB_TOKEN` environment variable through the CI/CD Variables settings.  This could be a group token if appropriate. If the `SEMANTIC_RELEASE_GITLAB_TOKEN` does not exist, `GITLAB_TOKEN` will be used.

```yaml
stages:
  - release

include:
  # Analyze commits to determine whether to cut a release
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/semantic-release.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.61.0  # renovate:managed
    file: 'semantic-release.yml'
```

## Examples of Conventional Commit Messages for Triggering Releases

Here are some examples of [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) messages for triggering a new release:

#### Fix, trigger a [Patch Release](https://semver.org/#spec-item-6)

With a `scope`:

```
fix(widget): correctly handle NPE in subtree explorer
```

Without a `scope`:

```
fix: correctly handle NPE in subtree explorer
```

#### New Feature: Trigger [a Minor Release](https://semver.org/#spec-item-7)

With a `scope`:

```
feat(widget): add the git subtree explorer
```

Without a `scope`:

```
feat: add the git subtree explorer
```

#### Breaking Change, trigger a [Major Release](https://semver.org/#spec-item-8)

```
feat: add the git subtree explorer

BREAKING CHANGES: git submodules explorer API removed.
```

## Delayed Releases

Some minor releases, for example dependency upgrades may need a new version release, but can be delayed to allow multiple upgrades to be included in the same release.

For this purpose, the `semantic_release` job will check for the string `[delay release]` (case-sensitive) in the commit-message. When added to the commit message, the release will be delayed by 1 hour, to allow other changes to be batched together, reducing release spinning.

#!/usr/bin/env bash

# This script will install the ASDF plugins required for this project

set -euo pipefail
IFS=$'\n\t'

# shellcheck source=/dev/null
source "$ASDF_DIR/asdf.sh"

plugin_list=$(asdf plugin list || echo)

install_plugin() {
  local plugin=$1

  if ! echo "${plugin_list}" | grep -q "${plugin}"; then
    echo "# Installing plugin" "$@"
    asdf plugin add "$@" || {
      echo "Failed to install plugin:" "$@"
      exit 1
    } >&2
  fi

  echo "# Installing ${plugin} version"
  asdf install "${plugin}" || {
    echo "Failed to install plugin version: ${plugin}"
    exit 1
  } >&2

  # Use this plugin for the rest of the install-asdf-plugins.sh script...
  asdf shell "${plugin}" "$(asdf current "${plugin}" | awk '{print $2}')"
}

remove_plugin_with_source() {
  local plugin=$1
  local source=$2

  if ! asdf plugin list --urls | grep -qF "${source}"; then
    return
  fi

  echo "# Removing plugin ${plugin} installed from ${source}"
  asdf plugin remove "${plugin}" || {
    echo "Failed to remove plugin: ${plugin}"
    exit 1
  } >&2

  # Refresh list of installed plugins.
  plugin_list=$(asdf plugin list)
}

# The location of the jsonnet-tool plugin has changed. Remove old
# plugin if it hasn't been migrated to the new source yet.
remove_plugin_with_source jsonnet-tool \
  https://gitlab.com/gitlab-com/gl-infra/asdf-jsonnet-tool.git

install_plugin golang # Install golang first as some of the other plugins require it.
install_plugin jb
install_plugin shellcheck
install_plugin shfmt
install_plugin pre-commit
install_plugin jsonnet-tool https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git
install_plugin yq
install_plugin goreleaser

# Environment Setup

This page describes the common process for managing dependencies using `asdf` that is used across many Infrastructure projects.

## Setup

### Step 1: Install 1password-cli v2

In order to protect sensitive values, many GitLab projects rely on 1password for storing secrets. This may be either directly, or through internal tooling such as [`pmv`](https://gitlab.com/gitlab-com/gl-infra/pmv).

Install 1password CLI using the instructions at: <https://developer.1password.com/docs/cli/get-started#install>. Unfortunately version management or package management tools cannot be used due to the security requirements of the 1password client.

For macos, you can use:

```shell
brew install --cask 1password/tap/1password-cli
```

Once 1password-cli is installed you can verify the installation using:

```shell
op --version
```

Note that v2 of the 1password-cli is required.

### Step 2: Setup Development Libraries

The Infrastructure group uses some Python-based tools, including the [`gcloud` CLI](https://cloud.google.com/sdk/gcloud) and the [AWS CLI](https://aws.amazon.com/cli/), [`pre-commit`](https://pre-commit.com/) and [Ansible](https://www.ansible.com/).

We've found that in some cases, Python will silently drop required modules (eg `sqlite3`) when the library dependencies of those modules are not installed on your system. This can lead to systems failing in unusual ways at runtime.

It is recommended that you follow the instructions to install the suggested build environment for Python, here: <https://github.com/pyenv/pyenv/wiki#suggested-build-environment>.

Linux users can follow the instructions for their distro on that site. For Macos users, the following set of commands should ensure the prerequisites.

```shell
# Ensure that xcode build tools are installed
xcode-select --install
# Ensure required libraries for python
brew install openssl readline sqlite3 xz zlib
```

### Step 2.5: Apple Silicon users only: install Rosetta

> To determine if you are using an Apple with Silicon or an Intel-based processor, select the **Apple** menu icon in the top-left of the desktop and click **About This Mac**. On Mac computers with Apple silicon, About This Mac shows an item labeled **Chip**, followed by the name of the chip (ex. **Chip** Apple M1 Pro). On Mac computers with an Intel processor, About This Mac shows an item labeled **Processor**, followed by the name of an Intel processor.

If you are running Apple Silicon it's highly recommended that you install Rosetta. Many downstream tools still generate `amd64` binaries only, and even if the tools support `arm64`, `asdf` plugins also need to support it, which they may not yet.

**Note**: You will likely hit many problems unless Rosetta is installed.

Check if Rosetta is installed by checking if it is running with the following one-liner:

```shell
pgrep -q oahd && echo "Rosetta is present on the system and is running." || echo "Rosetta is not running."
```

If it is not running, it is likely that it is not installed, so continue with the installation steps:

```shell
softwareupdate --install-rosetta --agree-to-license
```

### Step 3: Install `asdf`

Installation instructions for `asdf` can be found at <https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies>.

If you're running on macos, the recommended approach is to use Homebrew:

```shell
brew install asdf
```

Linux users should follow the instructions for their package manager in [the ASDF documentation](https://asdf-vm.com/guide/getting-started).

#### Step 3.5: Hook `asdf` into your shell

Once you've installed `asdf`, add the following line to your shell. Remember to restart your terminal for the change to take affect.

### Macos with Homebrew

```shell
. $(brew --prefix asdf)/asdf.sh
```

### Linux

```shell
. <path to asdf install>/asdf.sh
```

Did you remember to restart your terminal? Good.

### Step 2: Install development dependencies

Install all the plugins by running:

```shell
./scripts/install-asdf-plugins.sh
```

This will install required `asdf` plugins, and install the correct versions of the development tools.

Note that after pulling changes to the repository, you may sometimes need to re-run `./scripts/install-asdf-plugins.sh` to update your locally installed plugins and tool-versions.

## Updating Tool Versions

We use CI checks to ensure that tool versions used in GitLab-CI and on developer instances don't go out of sync.

### Keeping Versions in Sync between GitLab-CI and `asdf`.

`asdf` (and `.tool-versions` generally) is the SSOT for tool versions used in this repository.
To keep `.tool-versions` in sync with `.gitlab-ci.yml`, there is a helper script,
`./scripts/update-asdf-version-variables`.

#### Process for updating a Tool Version

```shell
./scripts/update-asdf-version-variables
```

1. Update the version in `.tool-versions`
1. Run `asdf install` to install latest version
1. Run `./scripts/update-asdf-version-variables` to update a refresh of the `.gitlab-ci-asdf-versions.yml` file
1. Commit the changes
1. A CI job (see [`asdf-tool-versions.md`](../asdf-tool-versions.md)) will validate the changes.

# Diagnosing `asdf` setup issues

## `asdf` plugins don't return versions as expected

To avoid installing dependencies, `asdf` plugins often rely on basic Unix text processing utilities like `grep`, `sed` and `awk` to parse JSON. Many rely on the fact that responses from the GitHub API are pretty-printed JSON, not minimised (or machine parsable) JSON. However, the GitHub API will only pretty-print JSON when it detects the User-Agent request header as being `curl`. For other user agents, the response will be minimised for efficiency.

Ensure that you haven't overridden your `curl` user-agent on your workstation.

Check your `.curlrc` for the `user-agent` setting. Additionally, running `curl https://api.github.com/orgs/gitlabhq` should return pretty-printed JSON. If the response contains minimised JSON, many `asdf` plugins may not work as expected.

## Plugins require other asdf tools

If you find that a plugin is failing to install, it sometimes helps to setup a global default. Care has been taken to avoid this situation, but if you're stuck, give it a try:

```shell
# If a plugin is complaining that it cannot compile because golang hasn't been configured...
# set the global version of golang the same as the current version
asdf global golang $(asdf current golang|awk '{ print $2 }')
asdf install
```

If this happens, please open an issue in the appropriate tracker, so that a better long-term solution can be applied.

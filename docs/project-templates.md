# Project Template and Boilerplate Management

As the number of GitLab Infrastructure projects grow, the toil involved in managing boilerplate rises too.

This project, `common-ci-tasks`, is intended to bring some level of uniformity between the various GitLab Infrastructure projects. It encourages convention-over-configuration as a way of allowing GitLab Infrastructure engineers to move between projects with a minimum of overhead. Additionally, cross-project source refactors and updates become easier.

## Project Templates using `copier`

In the effort to encourage uniformity in project layouts, the <https://gitlab.com/gitlab-com/gl-infra/common-template-copier> provides a basic project template. It uses the [`copier`](https://copier.readthedocs.io/en/latest/) project to manage template generation.

Consult the [`README.md`](https://gitlab.com/gitlab-com/gl-infra/common-template-copier/-/blob/main/README.md) document in the `common-template-copier` project for details of using Copier to create a new project from the template.

## Updating Existing Projects to Support `copier` Templates

Existing projects can be retrofitted to apply `copier` templates.

* Step 1: Install `copier`

```shell
pip3 install copier
```

* Step 2: Apply the template using the following command.

```shell
copier copy https://gitlab.com/gitlab-com/gl-infra/common-template-copier.git .
```

* Step 3: Answer the questions, and allow `copier` to overwrite any files.
* Step 4: Review git changes and amend diffs to ensure valid project changes are re-incorporated.
* Step 5: Check in the changes.

## Automated Boilerplate Updates

The default renovate configuration, [`renovate-common.json`](../renovate-common.json), is configured to propose template updates. This is done by monitoring the `.copier-version` file, along with the [Renovate Regex Manager](https://docs.renovatebot.com/modules/manager/regex/).

For this reason, it's recommended that new boilerplate is added to the [gitlab-com/gl-infra/common-template-copier](https://gitlab.com/gitlab-com/gl-infra/common-template-copier) first. When these changes are merged to main, and a new [semantic release](../semantic-release.md) is cut, Renovate will detect the new tag and automatically upgrade projects to incorporate the new boilerplate changes.

This is done using the [`copier update`](https://copier.readthedocs.io/en/latest/updating/) command, which will apply delta patches based on changes between the templates.

Once this is done, a new MR will be opened on each downstream project, incorporating the new boilerplate changes.
